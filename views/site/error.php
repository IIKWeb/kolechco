<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?php if (is_string($message)): ?>
            <?= nl2br(Html::encode($message)) ?>
        <?php elseif (is_array($message)): ?>
            <?php foreach ($message as $string): ?>
                <? print_r($string); ?>
                <br/>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</div>
