<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

?>
<div class="content_container" id="payments_page">
    <div id="center">
        <h2><a name="howWeWork"></a>Как мы работаем:</h2>

        <div id="weWork">
            <div id="itemProblem">
                <?= Html::img('/images/payments/imgArrow.png', ['class' => 'imgArrow']); ?>
                <p>
                    <?= Html::img('/images/payments/work-1.png'); ?>
<!--                    <img src="img/work-1.png"></p>-->

                <p>Выбираем понравившееся вам украшение</p>
            </div>
            <div id="itemProblem">
                <?= Html::img('/images/payments/imgArrow.png', ['class' => 'imgArrow']); ?>

                <p>
                    <?= Html::img('/images/payments/work-2.png'); ?>
<!--                    <img src="img/work-2.png">-->
                </p>

                <p>Оформляем заявку
                    на его покупку</p>
            </div>
            <div id="itemProblem">
                <?= Html::img('/images/payments/imgArrow.png', ['class' => 'imgArrow']); ?>

                <p>
                    <?= Html::img('/images/payments/work-3.png'); ?>
<!--                    <img src="img/work-3.png">-->
                </p>

                <p>Выставляем вам счет,
                    оплата через
                    карточку, банк или
                    при получении.</p>
            </div>
            <div id="itemProblem">
                <?= Html::img('/images/payments/imgArrow.png', ['class' => 'imgArrow']); ?>

                <p>
<!--                    <img src="img/work-4.png">-->
                    <?= Html::img('/images/payments/work-4.png'); ?>
                </p>

                <p>Доставка товара к Вам в город.</p>
            </div>
            <div id="itemProblem">
                <p>
<!--                    <img src="img/work-5.png"> -->
                    <?= Html::img('/images/payments/work-5.png'); ?>
                </p>

                <p>Вы счастливый обладатель украшений.</p>
            </div>
        </div>

        <h2><em>Как купить украшения в интернет магазине "KOLECHCO" &nbsp;легко и быстро!&nbsp;</em></h2>

        <p><big>&nbsp; Вы имеете возможность оплатить&nbsp;понравившиеся&nbsp;Вам&nbsp;украшения&nbsp;удобным&nbsp;&nbsp;
                для Вас способом. Доставка&nbsp;заказанной Вами продукции производится курьером в удобное для Вас время
                по Киеву и до 2-х рабочих суток по Украине. Заказы в интернет магазине «KOLECHCO» онлайн можно делать
                круглосуточно! Если у Вас возникли вопросы Вы можете обратится по тел. +38 (068) 574 47 03 или заказать
                "обратный зонок" и мы свяжемся с Вами в ближайшее время.</big></p>

        <p><big><strong>&nbsp; Доставка осуществляется во все области и города&nbsp;Украины:&nbsp;</strong>Винницкая
                область, Волынская область, Днепропетровская область,<a
                    href="http://prostoblesk.com.ua/donetsk-bizhuteriya-ukrasheniya-aksessuari">&nbsp;Донецкая
                    область</a>, Житомирская область, Закарпатская область, Запорожская область, Ивано-Франковская
                область,<a href="http://prostoblesk.com.ua/kiev-bizhuteriya-ukrasheniya-aksessuari">&nbsp;Киев</a>,&nbsp;<a
                    href="http://prostoblesk.com.ua/kiev-bizhuteriya-ukrasheniya-aksessuari">Киевская область</a>,
                Кировоградская область, Луганская область, Львовская область,&nbsp;<a
                    href="http://prostoblesk.com.ua/nikolaev-bizhuteriya-ukrasheniya-aksessuari">Николаев</a>,&nbsp;Николаевская
                область, Одесская область, Полтавская область, Ровненская область, Севастополь, Сумская область,
                Тернопольская область, Харьковская область, Херсонская область, Хмельницкая область, Черкасская область,
                Черниговская область, Черновицкая область.</big></p>

        <h2>Способы оплаты: приват 24,&nbsp;наложенный платеж</h2>

        <p>
            <?= Html::img('/images/payments/privat_24.jpg',['style' => "height:72px; width:237px"]); ?>
<!--            <img alt="" src="http://kolechco.com.ua/img/privat_24.jpg" style="height:72px; width:237px">-->
            <?= Html::img('/images/payments/nova_poshta_nalog.jpg'); ?>
<!--            <img alt="" src="http://kolechco.com.ua/img/nova_poshta_nalog.jpg">-->
        </p>

        <p><big><strong>✔&nbsp;Наличный расчет</strong>&nbsp;- возможен при оплате заказа наличными деньгами в таких
                случаях. Осуществляется в период доставки товара курьером по городу Киев, а также если Вы решили
                оплатить товар с помощью наложеного платежа при получении товара на Новой почте</big></p>

        <p><big>✔&nbsp;Оплата системами&nbsp;<strong>Приват 24, LIQPAY.</strong></big></p>

        <h2>Доставка по Украине</h2>

        <p>
            <?= Html::img('/images/payments/nova_poshta.jpg'); ?>
<!--            <img alt="" src="http://kolechco.com.ua/img/nova_poshta.jpg">-->
        </p>

        <p><big><strong>►&nbsp;Новая почта:&nbsp;</strong>Доставка производится до отделения курьерской службы или
                непосредственно «до двери» клиента.&nbsp;Список представительств курьерской службы по Украине можно
                посмотреть на сайте http://novaposhta.ua/.&nbsp;Также здесь возможен расчет стоимости доставки
                продукции.&nbsp;Ее отправка будет осуществляться на следующий день после того, как был сделан заказ.
                Доставка занимает 1-3 суток.</big></p>

        <p>&nbsp;</p>

        <h2><em><big>Курьерская доставка по Киеву</big></em></h2>

        <p>&nbsp;</p>

        <p><big>&nbsp; Осуществляется курьером в офис или на дом покупателя.</big></p>

        <p><big>Либо же в любом удобном для Вас месте.</big></p>

        <h2><em>&nbsp;<big>Гарантия/сервис</big></em></h2>

        <p><big>Вы можете вернуть или обменять товар в течении 14 дней</big></p>

        <p>&nbsp;</p>

        <p>&nbsp;</p>
    </div>
</div>