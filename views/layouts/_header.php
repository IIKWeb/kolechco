<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

?>

<header>
    <div id="infoShop">
        <p class="name">www.kolechco.com.ua</p>

        <p class="slogan">Интернет магазин бижутерии</p>
    </div>
    <div id="contacts">
        <p><strong>Есть вопросы? Звоните!</strong></p>

        <p class="phone">+38 (068) 574 47 03</p>
        <a href="#orderCall" class="orderCall">
            <?= Html::img('/images/header/iPhone.png'); ?>Заказать звонок</a>
        <a href="#" class="overlay" id="orderCall"></a>

        <div class="popup">
            <h3>Заказать звонок:</h3>

            <div id="formOrderCall">
                <form name="contact_form" method="post" action="send_email.php">
                    <input type="text" name="name" placeholder="Имя"
                           onfocus="if (this.value == 'Имя') {this.value = '';}"
                           onblur="if (this.value == '') {this.value = 'Имя';}" required="">
                    <input type="tel" name="namber" placeholder="Телефон"
                           onfocus="if (this.value == 'Телефон') {this.value = '';}"
                           onblur="if (this.value == '') {this.value = 'Телефон';}" required="">
                    <input type="submit" value="Заказать звонок" class="butOrderCall">
                </form>
            </div>
            <p>Оставьте свой номер телефона и наш менеджер свяжется с Вами в ближайшее время.</p>
            <a class="close" href="#close"></a>
        </div>
    </div>
    <div id="description">
        <h2>Качественная итальянская бижутерия!</h2>

        <p><strong>Внешний вид золота, не боится воды, не темнеет, не облазит, гипоаллергенна. </strong></p>

        <p>Доставка по всей Украине от 3 до 48 часов.</p>
    </div>
    <div id="status">


        <div id="basket">
            <a href="#carForm" class="carButtom">
                <?= Html::img('/images/header/iPhone.png'); ?>Корзина</a>

            <div id="info"><p>Товаров - 0</p>

                <p>На сумму - 0 грн.</p>
            </div>

            <a href="#x" class="overlay" id="carForm"></a>

            <div class="popup" style="overflow: hidden; height: 70%; overflow-x:hidden; ">
                <h3>Корзина:</h3>

                <div id="goods">В корзине нет товаров</div>
                <a class="close" href="#close"></a></div>
        </div>
    </div>
</header>
<p class="boxShadow">
    <?= Html::img('/images/header/boxShadow.png'); ?>
</p>
<menu>
    <ul class="mainMenu">
        <li><a href="/">Главная </a></li>
        <li><a href="/payments">Оплата и доставка </a></li>
        <li><a href="/about">О нас </a></li>
        <li><a href="/contacts">Контакты</a></li>

    </ul>
</menu>