<div id="left">
    <form id="search" method="get" action="product.php">
        <input type="text" name="search" placeholder="Код изделия...">
        <input type="submit" value="Поиск" class="searchButton">
    </form>

    <div id="menuTov">
        <h1>Каталог:</h1>
        <ul>
            <li><a href="product.php?product=ring">Кольца</a></li>
            <li><a href="product.php?product=earring">Серьги</a></li>
            <li><a href="product.php?product=pendent">Подвески</a></li>
            <li><a href="product.php?product=chainlet">Цепочки</a></li>
            <li><a href="product.php?product=brooch">Брошь</a></li>

            <!--<li><a href="product.php?product=pin">Брошка</a></li>
            <li><a href="product.php?product=carbines">Карабин</a></li>-->
        </ul>
    </div>

    <div id="reviews">
        <div id="itemReviews">
            <p class="image"><img src="avatars/14067150101237.jpg" width="170" height="170"></p>

            <p class="name">Алексей Шешко</p>

            <p class="city">г. Киев</p>

            <p class="text">Впервые воспользовался в марте 2013 года для подарка девушке.
                Понравилось то что все изделии оригинальные и интересны по своему. Очень качественная бижутерия за
                достаточно доступную цену
                Рекомендую всем кто любит хорошие, качественные и по доступной цене украшения
                Очень удивили цепочки на метраж. Это что то новое на рынке бижутерии. Не ожидал что могут получиться
                такие прикольные изделия.
                Пришло понимание того что есть продукция высокого качества за доступную цену
            </p>

            <p class="vk"><img src="img/vk.png"><a href="https://vk.com/a_sheshko" target="_blank">https://vk.com/a_sheshko</a>
            </p>
        </div>
        <div id="cell"></div>
        <div id="itemReviews">
            <p class="image"><img src="avatars/14067150107918.jpg" width="170" height="170"></p>

            <p class="name">Майя Якушева</p>

            <p class="city">г. Шостка</p>

            <p class="text">Первый раз воспользовались год назад. Понравилась стойкость материала к окружающей среде-
                это главное преимущество перед остальной бижутерией на рынке Украины. Рекомендуем всем. Очень красивая и
                качественная бижутерия. Бижутерия из сплава элоксал полностью перевернула мое представление о бижутерии.
                Очень довольна</p>

            <p class="vk"><img src="img/vk.png"><a href="https://vk.com/id12281182" target="_blank">https://vk.com/id12281182</a>
            </p>
        </div>
        <div id="cell"></div>
        <div id="itemReviews">
            <p class="image"><img src="avatars/14067149603447.jpg" width="170" height="170"></p>

            <p class="name">Артем Дивертито</p>

            <p class="city">г. Киев</p>

            <p class="text">Впервые столкнулся с Элоксалом, где-то в феврале - марте 2013.
                В первую очередь понравилось, что это была новинка, в плане представленности и качества продукции в
                целом. Аналоги золота были и есть всегда, но подобного качества материала, пока в продаже не было.
                Порекомендовал бы людям, которые любят качественные украшения за доступную цену. Ещё понравилось то, что
                многие думали что я ношу золотые изделия, но каково было их удивления, когда они узнавали что это
                бижутерия))))
            </p>

            <p class="vk"><img src="img/vk.png"><a href="http://vk.com/divertitto" target="_blank">http://vk.com/divertitto</a>
            </p>
        </div>
    </div>
    <!-- OFORMIT -->
    <a href="#x" class="overlay" id="issue"></a>

    <div class="popup" style="overflow: auto; height: 85%; overflow-x:hidden;" id="issue">
        <h3>Оформить заказ:</h3>

        <div id="issueForm">
            <form name="contact_form" method="post" action="send_email.php">
                <input type="text" name="fam" placeholder="Фамилия"
                       onfocus="if (this.value == 'Фамилия') {this.value = '';}"
                       onblur="if (this.value == '') {this.value = 'Фамилия';}" required="">
                <input type="text" name="name" placeholder="Имя" onfocus="if (this.value == 'Имя') {this.value = '';}"
                       onblur="if (this.value == '') {this.value = 'Имя';}" required="">
                <input type="tel" name="namber" placeholder="Телефон"
                       onfocus="if (this.value == 'Телефон') {this.value = '';}"
                       onblur="if (this.value == '') {this.value = 'Телефон';}" required="">
                <input type="text" name="city" placeholder="Город"
                       onfocus="if (this.value == 'Город') {this.value = '';}"
                       onblur="if (this.value == '') {this.value = 'Город';}" required="">

                <p class="waypaid">Выбор способа оплаты:</p>

                <div class="styled-select" style="width: 265px;">
                    <select name="sposob" style="width: 290px;">
                        <option value="1">Карта Visa / Mastercard (Приват 24)</option>
                        <option value="2">Наложенный платеж</option>
                    </select>
                </div>

                В корзине нет товаров
            </form>
        </div>

        <div id="goods">
            <input type="hidden" name="operation_xml"
                   value="PHJlcXVlc3Q+ICAgICAgCiAgICA8dmVyc2lvbj4xLjI8L3ZlcnNpb24+CiAgICA8cmVzdWx0X3VybD5odHRwOi8va29sZWNoY28uY29tLnVhLyNwYXk8L3Jlc3VsdF91cmw+CiAgICA8c2VydmVyX3VybD5odHRwOi8va29sZWNoY28uY29tLnVhL3BheW1lbnQucGhwPC9zZXJ2ZXJfdXJsPgogICAgPG1lcmNoYW50X2lkPmk0MDEyNDE4OTA3PC9tZXJjaGFudF9pZD4KICAgIDxvcmRlcl9pZD5PUkRFUl88L29yZGVyX2lkPgogICAgPGFtb3VudD4wPC9hbW91bnQ+CiAgICA8Y3VycmVuY3k+VUFIPC9jdXJyZW5jeT4KICAgIDxkZXNjcmlwdGlvbj48L2Rlc2NyaXB0aW9uPgogICAgPGRlZmF1bHRfcGhvbmU+KzM4MDk1NjA3MjczMDwvZGVmYXVsdF9waG9uZT4KICAgIDxwYXlfd2F5PmNhcmQsIGxpcXBheSwgZGVsYXllZDwvcGF5X3dheT4gCiAgICA8L3JlcXVlc3Q+CiAgICA=">
            <input type="hidden" name="signature" value="NoZQ409t7VThTvQcN1baRWn6+hk=">
            <input type="hidden" name="buy" value="https://www.liqpay.com/?do=clickNbuy">
            <input type="hidden" name="new_cena" value="0">
            <input type="hidden" name="tovar" value="">
        </div>
        <a class="close" href="#close"></a>


    </div>
</div>