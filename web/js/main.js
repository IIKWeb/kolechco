$(document).ready(function () {
    if ($("html").find("#feedback").length == 1) {
        /** Feedback form submit handler */
        $(this).on('submit', '#feedback-form', function (e) {
            e.preventDefault()
            var form = $(this).closest('form');
            var formData = form.serialize();

            $.ajax({
                url: form.attr('action'),
                method: 'post',
                data: formData,
                dataType: 'json',
                success: function (data) {
                    //console.log(data);
                    if (data.status == 'success') {
                        location.href = '/feedback/thanks';
                    } else {

                        $('.text-danger').remove();

                        var mess = '';
                        $.each(data.message, function (k, v) {
                            $.each(v, function (k1, v1) {
                                form.find('button[type=submit]').before('<p class="text-danger" >' + v1 + '</p>')
                            });
                        });
                    }
                }
            });

        });
    }

//
//    /*<![CDATA[*/
//    /*
//     ���������� ������ �� �������� HTML ������.
//
//     �������� ���� ������ ���������� � ��������� � ��� �����.
//     ������� ���������� ��������� ����� ������ ��� ����� �� ���.
//     - �������� ��������� ������� ��������� (������/������).
//     - ���� ���������� � ���� ������ ����, �� ����� ���������������
//     ��� �������������, � ����������� �� �������� ���������.
//     - ��� �������� � ������ ���� �� ������ ��������� ��������� (.current)
//     � ������� �� ��������� ����.
//     - ���������� ��������� ���� � ���������� � �������� ��������������
//     ����� �� ���������� ���� ����� ������.
//     */
//    $(document).ready(function () {
//        /* ����������� ������� �� �����, ������ ������ ���� ���������.
//         �������� �������� 'li' ������� ����� ��������� 'ul', ������ ��� ���
//         ������, �.�. ������� � ���� 'li' ��������� ��� 'a'
//         � � ���� ���������� ������ '<em class="marker"></em>'.
//         a:first ������������, ����� ����� ���� 1�� ������ �����������
//         ������� �� ����������� ��������.
//         */
//        $('#multi-derevo li:has("ul")').find('a:first').prepend('<em class="marker"></em>');
//// ������ ������� �� ���� �� ������
//        $('#multi-derevo li span').click(function () {
//            // ������� ��������� ����������� ����
//            $('a.current').removeClass('current');
//            var a = $('a:first',this.parentNode);
//            // �������� ��������� ����
//            //���� a.hasClass('current')?a.removeClass('current'):a.addClass('current');
//            a.toggleClass('current');
//            var li=$(this.parentNode);
//            /* ���� ��� ��������� ���� ������, �� �������������� ����� � ����������
//             �������� �� ����� */
//            if (!li.next().length) {
//                /* ����� ������ ������������ <li>, � ��� ������� ��������� <ul>,
//                 �������� ������ �������� ul > li, ��������� �� ����� 'last' */
//                li.find('ul:first > li').addClass('last');
//            }
//            // �������� ��������� ���� � ��������� ��������� �������
//            var ul=$('ul:first',this.parentNode);// ������� ���������
//            if (ul.length) {// ��������� ����
//                ul.slideToggle(300); //�������� ��� ����������
//                // ������ ���������� ������� �� �������/�������
//                var em=$('em:first',this.parentNode);// this = 'li span'
//                // ���� em.hasClass('open')?em.removeClass('open'):em.addClass('open');
//                em.toggleClass('open');
//            }
//        });
//    })
//    /*]]>*/
});