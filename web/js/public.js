$(document).ready(function () {
    $('#goals').pinto({
        itemSelector: '.goal_wrapper',
        itemWidth: 260,
        gapX: 30,
        gapY: 30,
    });
    $(window).resize();
    setTimeout(function(){ $(window).resize(); }, 2000);
});