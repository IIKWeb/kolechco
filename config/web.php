<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'uk-UK',
    'modules' => [
        'admin' => [
            'class' => 'app\module\admin\AdminPanel',
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'VrUErQp00983_xmETUyjTuWot-AvX5OT',
            'baseUrl' => '',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'transport' => [
            'class' => 'Swift_SmtpTransport',
            'host' => 'stella.rappukraine.com',
            'username' => 'mailaccount@gmail.com',
            'password' => 'yourpassword',
            'port' => '465',
            'encryption' => 'ssl',

        ],
        //
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                'payments' => 'payments/index',
                'about' => 'about/index',
                'contacts' => 'contacts/index',
            ],
        ],
//        'db' => require(__DIR__ . '/db.php'),
        'db' => array_merge(
            require(__DIR__ . '/db.php'),
            require(__DIR__ . '/db-local.php')
        ),
//        'crm' => require(__DIR__.'/crm.php'),
//        'crm_et' => require(__DIR__.'/crm_exet_target.php'),
        'i18n' => array(
            'translations' => array(
                'app' => array(
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => "@vendor/yiisoft/yii2/messages",
                    'sourceLanguage' => 'en_US',
                    'fileMap' => array(
                        'app' => 'app.php',
                        'authorization' => 'authorization.php'
                    )
                ),
                'yii' => array(
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => "@vendor/yiisoft/yii2/messages",
                    'sourceLanguage' => 'en_US',
                    'fileMap' => array(
                        'yii' => 'yii.php',
                        'authorization' => 'authorization.php',
                        'app' => 'app.php',
                    )
                )
            )
        ),


//        'assetManager' => [
//            'basePath' => '@webroot/assets',
//            'baseUrl' => '@web/assets'
//        ],
    ],
    'params' => $params,

];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
